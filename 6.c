#include <stdio.h>
#include <malloc.h>

int main() {
    int arr[4], *m;
    m = arr;
    arr[0] = 1000;
    arr[1] = 1001;
    arr[2] = 1002;
    arr[3] = 1003;
    printf("%d %d %d %d", *m, *(m + 1), *(m + 2), *(m + 3));
    printf("\n\n");

    int *a;
    a = (int *) malloc(4 * sizeof(int));
    for (int i = 0; i < 4; i++) {
        printf("%d", a[i]);
    }
    free(a);
    return 0;
}

