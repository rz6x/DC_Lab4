#include<stdio.h>
#include<string.h>

int main() {
    char str1[100];
    char str2[90];
    char s;
    scanf(" %[^\n]s\n", str1);
    scanf(" %[^\n]s\n", str2);
    //1
    {
        printf("Task 1\n");
        printf("%s%s\n\n", str1,str2);
    }
    //2
    {
        printf("Task 3\n");
        if (strcmp(str1, str2) == 0)
            printf("0\n\n");
        else
            printf("1\n\n");
    }
    //3
    {
        printf("Task 8\n");
        fflush(stdin);
        printf("Put symbol: \n");
        scanf("%c", &s);
        if(strchr(str1, s)==NULL)
            printf("No\n");
        else
            printf( "Symbol is at position: %lld\n\n", (strchr(str1, s) - str1 + 1));
    }
    //4
    printf("Task 10\n");
    int f = 0;
    for (int i = 0; str1[i] != '\0' && !f; i++)
    {
        for (int j = 0; str2[j] != '\0' && !f; j++)
        {
            if (str1[i] == str2[j])
            {
                printf("Character: %c\n\n", str1[i]);
                f = 1;
            }
        }
    }
    //5
    printf("Task 12\n");
    int length;
    length = strcspn(str1, str2);
    printf("Character where strings intersect is at position %d\n", length);

    return 0;
}