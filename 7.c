#include <stdio.h>
#include <locale.h>

enum Lamps
{
    incandescent,
    daylight,
    halogen
};
struct triangle
{
    int side1;
    int side2;
};
union Cardreader
{
    int value;
    struct
    {
        unsigned CardR : 1;
        unsigned SD : 1;
        unsigned CompactFlash : 1;
        unsigned MemoryStick : 1;
    };
};
int main()
{
    setlocale(LC_ALL, "Ru");
    printf("Task 1\n");
    enum Lamps s = halogen;
    printf("%d\n", s);
    printf("\n");

    printf("Task 2\n");
    struct triangle s1;
    printf("Side1 = ");
    scanf("%d", &s1.side1);
    struct triangle s2;
    printf("Side2 = ");
    scanf("%d", &s2.side2);
    printf("S=%d\n", (s1.side1 + s2.side2)/2);

    printf("Task 3\n");
    union Cardreader cr;
    printf("Enter a number in hexadecimal notation: ");
    scanf("%x", &cr.value);
    printf("CardR: %s || \n SD: %s || \n CompactFlash: %s || \n MemoryStick: %s\n",
           cr.CardR ? "ON" : "OFF",
           cr.SD ? "ON" : "OFF",
           cr.CompactFlash ? "ON" : "OFF",
           cr.MemoryStick ? "ON" : "OFF");
    return 0;
}