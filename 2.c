#include <stdio.h>
#include <math.h>

int main() {
    float x,y,z1,z2;
    scanf("%f%f", &x, &y);
    z1 = pow(cosf(x), 4.0) + pow(sinf(y), 2.0) + 1.0/4.0 * pow(sinf(2.0*x), 2.0) - 1.0;
    z2 = sinf(y+x) * sinf(y-x);
    printf("%f\n%f\n", z1,z2);
    return 0;
}
